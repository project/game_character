
CONTENTS OF THIS FILE
---------------------

 * Introduction and Initial Design Goals
 * Installation


INTRODUCTION AND INITIAL DESIGN GOALS
=====================================

The Game Character module provides a structure and API for Game Character nodes
on your site. You can set one or more node types to be Game Character types
from the Game Character administration page, at admin/settings/game_characters.
At that point, any nodes of that type will be set as game characters, which
may be activated by the owning user, or any user with administrative
permissions.

Characters may be activated or inactivated with provided links. Additionally,
if the node is created by an anonymous user, and that user later logs in, they
may be provided with a unique 'claim character' link which will change
ownership of the node.

By itself, this API currently does little more. However, it may be used in
conjunction with other modules. See the project page for other modules
currently using the Game Character API.


INSTALLATION
============

1. Copy the files to your sites/SITENAME/modules directory.
   Or, alternatively, to your sites/all/modules directory.

2. Enable the Game Character module at admin/build/modules.

3. You can configure Game Characters at admin/settings/game_characters.
   Make sure to enable at least one game character node type from this page.

4. Set any required permissions at admin/user/permissions.

5. To create a character, head to node/add/[node-type].

