<?php

/**
 * @file
 * Admin functions for the Game Character module.
 */

/**
 * Configure Game Character; menu callback for admin/settings/game_character.
 */
function game_character_settings() {
  $form = array();

  $types = node_get_types('names');

  $form['nodes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Game character types'),
    '#collapsible' => TRUE,
  );
  $form['nodes']['game_character_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Game character types'),
    '#default_value' => variable_get('game_character_types', array()),
    '#options' => $types,
    '#description' => t("Select the node type(s) that will be used for characters."),
  );

  $form['active'] = array(
    '#type' => 'fieldset',
    '#title' => t('Active characters'),
    '#collapsible' => TRUE,
  );
  $form['active']['game_character_multiple_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Multiple active'),
    '#description' => t('If checked, then users may select more than one game character at a time.'),
    '#default_value' => variable_get('game_character_multiple_active', TRUE),
  );
  $form['active']['game_character_store_active_in_session'] = array(
    '#type' => 'checkbox',
    '#title' => t('Store active in session'),
    '#description' => t('If checked, then store the active character in the session, rather than soley in the database. This allows a user to control different characters in different sessions. (Must be checked to allow anonymous users to have active characters.)'),
    '#default_value' => variable_get('game_character_store_active_in_session', TRUE),
  );

  $form['claims'] = array(
    '#type' => 'fieldset',
    '#title' => t('Game character claims'),
    '#collapsible' => TRUE,
  );
  $form['claims']['game_character_allow_claims'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow game character claims to select node'),
    '#description' => t('If checked, then when anonymous users create a game character node, a resulting unique claim URL will be displayed, allowing the user to select that character in the future.'),
    '#default_value' => variable_get('game_character_allow_claims', TRUE),
  );
  $form['claims']['game_character_allow_claim_ownership'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow game character claims to change node ownership'),
    '#description' => t('If checked, then when anonymous users create a game character node, a resulting unique claim URL will be displayed, allowing the user to claim ownership of the node when logging in with a user role allowing the possibility.'),
    '#default_value' => variable_get('game_character_allow_claim_ownership', TRUE),
  );

  return system_settings_form($form);
}
