<?php

function game_character_character_activate($node) {
  global $user;
  game_character_set_active($user->uid, $node);
  drupal_set_message(t("The game character %title is now active.", array('%title' => $node->title)));
  drupal_goto('node/'. $node->nid);
}

function game_character_character_inactivate($node) {
  global $user;
  db_query("DELETE FROM {game_characters} WHERE nid=%d AND uid=%d", $node->nid, $user->uid);
  unset($_SESSION['game_character_active']);
  drupal_set_message(t("The game character %title is now inactive.", array('%title' => $node->title)));
  drupal_goto('node/'. $node->nid);
}

/**
 * @file
 * Page callbacks for the Game character module.
 */

/**
 *  Page callback for game_character/claim/%node/[$claim].
 *  If the user is anonymous, this will set the user's active character as
 *  the claimed character, if the system allows.
 *  If the user has the permission to claim the character, and the system
 *  allows, it will also set the ownership of the node to that user.
 *  @param $node
 *    The node to be claimed. It is already checked for validity in the menu
 *    callback process, so will be denied if this is not a game character
 *    node, or the user is unable to play game characters.
 *    Note the node must also be owned by an anonymous user.
 *  @param $claim
 *    (optional) If not given, this defaults to the claim in the database for
 *    the user's session variable.
 *    This must be a hash code presented at the time of node creation,
 *    or displayed by a game character administrator. Otherwise, it will be
 *    access denied.
 */
function game_character_character_claim($node, $claim = NULL) {
  global $user;

  // If no claim code is given, one last chance given if we have the character active in session.
  if (is_null($claim) && variable_get('game_character_store_active_in_session', TRUE)) {
    $active = $_SESSION['game_character_active'];
    $claim = db_result(db_query("SELECT claim FROM {game_character_claims} WHERE nid=%d", $active));
  }

  // The claim code must match what's in the database.
  if ($claim != db_result(db_query("SELECT claim FROM {game_character_claims} WHERE nid=%d", $node->nid))) {
    drupal_access_denied();
  }

  // Transfer ownership of the node if allowed, and the user is logged in.
  if (variable_get('game_character_allow_claim_ownership', TRUE) && $user->uid) {
    $node->uid = $user->uid;
    node_save($node);
    game_character_set_active($user->uid, $node);
    drupal_set_message(t("You have claimed the game character %title as your own.", array('%title' => $node->title)));
    drupal_goto('node/'. $node->nid);
  }

  // Simply set the character as active for the user if we're still here and allowed.
  if (variable_get('game_character_allow_claims', TRUE)) {
    game_character_set_active($user->uid, $node);
    drupal_set_message(t("%title is now your active game character.", array('%title' => $node->title)));
    drupal_goto('node/'. $node->nid);
  }

  // No can do.
  drupal_access_denied();
}
